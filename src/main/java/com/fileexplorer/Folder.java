package com.fileexplorer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Folder extends AFile {
    private List<AFile> files;

    public Folder(File file) {
        super(file);
        explore();
    }

    /**
     * Returns only files and not folders.
     * 
     * @return
     */
    public List<AFile> getFileOnlyList() {
        List<AFile> files = new ArrayList<AFile>();
        for (AFile entry : this.getFiles()) {
            if (entry instanceof Folder) {
                files.addAll(((Folder) entry).getFileOnlyList());
            } else {
                files.add(entry);
            }
        }
        return files;
    }

    public List<AFile> getFiles() {
        return files;
    }

    public void explore() {
        this.files = new ArrayList<AFile>();
        File file = this.getFile();
        if (file != null && file.isDirectory()) {
            for (File entry : file.listFiles()) {
                if (entry != null) {
                    if (entry.isDirectory()) {
                        Folder folder = new Folder(entry);
                        this.files.add(folder);
                    } else if (entry.isFile()) {
                        AFile aFile = new AFile(entry);
                        this.files.add(aFile);
                    }
                }
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getName());

        for (AFile aFile : this.getFiles()) {
            sb.append(aFile.toString());
        }

        return sb.toString();
    }
}
