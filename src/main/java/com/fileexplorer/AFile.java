package com.fileexplorer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AFile {
    private final File file;

    public AFile(File file) {
        this.file = file;
    }

    public String getName() {
        return file != null ? file.getName() : "<no name>";
    }

    public File getFile() {
        return file;
    }

    public List<AFile> getFileOnlyList() {
        List<AFile> files = new ArrayList<AFile>(1);
        files.add(this);
        return files;
    }
    
    public boolean hasSameName(AFile aFile) {
        return (aFile != null && aFile.getName().equals(getName()));
    }

    public String toString() {
        return this.getName();
    }

}
