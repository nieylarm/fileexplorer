package com.fileexplorer;

import java.io.File;
import java.util.List;

public class FileExplorer {

    private final AFile aFile;

    public FileExplorer(String path) {
        if (path != null && path.length() > 0) {
            File file = new File(path);
            if (file != null) {
                this.aFile = createAFile(file);
                return;
            }
        }
        this.aFile = null;
    }

    public FileExplorer(File file) {
        if (file != null) {
            this.aFile = createAFile(file);
            return;
        }
        this.aFile = null;
    }

    public AFile getaFile() {
        return aFile;
    }

    public String generateFileList() {
        return generateFileList(aFile);
    }

    protected String generateFileList(AFile aFile) {
        StringBuilder sb = new StringBuilder(1024);
        if (aFile instanceof Folder) {
            Folder folder = (Folder) aFile;

            sb.append(folder.getName());
            sb.append("\n");

            List<AFile> files = folder.getFiles();
            for (AFile entry : files) {
                sb.append(generateFileList(entry));
            }
        } else {
            sb.append(aFile.getName());
            sb.append("\n");
        }
        return sb.toString();
    }

    public void trace() {
        trace(this.aFile);
    }

    protected void trace(AFile aFile) {
        // StringBuilder sb = new StringBuilder(1024);
        if (aFile instanceof Folder) {
            // trace it
            Folder folder = (Folder) aFile;

            // sb.append(folder.getName());
            System.out.println(folder.getName());

            List<AFile> files = folder.getFiles();
            for (AFile entry : files) {
                trace(entry);
            }
        } else {
            System.out.println(aFile.getName());
        }
    }

    protected AFile createAFile(File file) {
        AFile aFile = null;
        if (file.isDirectory()) {
            aFile = new Folder(file);
        } else if (file.isFile()) {
            aFile = new AFile(file);
        }
        return aFile;
    }

    public static void main(String[] args) {
        String path = "C:\\data";
        FileExplorer explorer = new FileExplorer(path);

        // generate all folders and files
        // String str = explorer.generateFileList();
        // System.out.println(str);

        AFile aFile = explorer.getaFile();
        // System.out.println(aFile);

        // generate files only
        List<AFile> files = aFile.getFileOnlyList();

        System.out.printf("There are %d number of files.\n\n", files.size());

        for (AFile entry : files) {
            System.out.println(entry);
        }
    }
}