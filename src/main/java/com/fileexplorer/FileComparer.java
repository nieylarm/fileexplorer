package com.fileexplorer;

import java.util.ArrayList;
import java.util.List;

public class FileComparer {

    private final AFile file1;
    private final AFile file2;

    public FileComparer(String p1, String p2) {
        FileExplorer explorer = new FileExplorer(p1);
        
        file1 = explorer.getaFile();
        
        explorer = new FileExplorer(p2);

        file2 = explorer.getaFile();
    }

    public void compare() {
        List<AFile> list1 = file1.getFileOnlyList();
        List<AFile> list2 = file2.getFileOnlyList();
        List<AFile> commons1 = new ArrayList<AFile>();
        List<AFile> commons2 = new ArrayList<AFile>();
        List<AFile> diff1 = new ArrayList<AFile>();
        List<AFile> diff2 = new ArrayList<AFile>();

        for (int i = 0; i < list1.size(); i++) {
            AFile e1 = list1.get(i);
            for (int j = i; j < list2.size(); j++) {
                AFile e2 = list2.get(j);
                if (e2.hasSameName(e1)) {
                    commons1.add(e1);
                    commons2.add(e2);
                    break;
                }
            }
        }
        
        // copy all lists to diff lists
        diff1.addAll(list1);
        diff2.addAll(list2);

        // remove common file from original lists
        for (AFile e : commons1) {
            diff1.remove(e);
        }
        for (AFile e : commons2) {
            diff2.remove(e);
        }

        // reporting
        System.out.printf("Total files 1: %d \n", list1.size());
        System.out.printf("Total files 2: %d \n", list2.size());
        System.out.printf("Common files 1: %d \n", commons1.size());
        System.out.printf("Common files 2: %d \n", commons2.size());
        System.out.printf("Diff in first list: %d \n", diff1.size());
        System.out.printf("Diff in second list: %d \n", diff2.size());
    }

    public static void main(String[] args) {
        String p1 = "C:\\data";
        String p2 = "C:\\data";

        FileComparer comparer = new FileComparer(p1, p2);

        comparer.compare();
    }
}
